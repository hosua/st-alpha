# st-alpha

My st build with the alpha patch applied. Does not work with w3m or ranger image previews.

To compile st, run ``sudo make clean install`` in the directory that you extracted the files.

## Patches applied
```
st-anysize-0.8.4
st-dracula-0.8.2
st-scrollback-20210507-4536f46
st-scrollback-mouse-20191024-a2c479c
dwm-alpha-20201019-61bb8b2
```

![image](st-alpha.png)
